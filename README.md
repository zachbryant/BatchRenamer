BatchRenamer uses regular expressions to aid in the process of renaming files.

Originally created to rename 280+ icons I downloaded for another project.

Just follow the prompts and know your regex :)