/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package batchrenamer;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author Zach
 */
public class BatchRenamer {

    /**
     * @param args the command line arguments
     */
    static Scanner scan = new Scanner(System.in);
    static boolean skipask;
    public static void main(String[] args) {
        System.out.print("Fully qualified path to file folder: ");
        String path = scan.nextLine();
        //Full path to folder
        File target = new File(path);
        while(!target.exists()){
            System.out.println("Target file path does not exist or cannot be read. Please enter a new one or enter \"q\" to quit");
            System.out.print("Fully qualified path to file folder: ");
            path = scan.nextLine();
            if(path.equalsIgnoreCase("q"))
                break;
            
            target = new File(path);
        }
        if(path.equalsIgnoreCase("q"))
                return;
        System.out.print("Enter regular expression: ");
        String regex = scan.nextLine();
        
        System.out.print("Enter file name replacement: ");
        String replace = scan.nextLine();
        if(replace == null || replace.length()<1)
            replace = "";
        rename(target,regex,replace);
    }
    public static void rename(File target,String regex, String replace){
        File[] listOfFiles = target.listFiles();

        if (listOfFiles != null) {
            bread:for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].isFile()) {
                    String name = listOfFiles[i].getName();
                    String result = name.replaceAll(regex,replace);
                    if(name.equals(result))
                        continue bread;
                    File f = new File(target.getPath(),name);
                    
                    if(!skipask){
                        System.out.print(name + " will be renamed to "+result+". Are you sure? Y/n/ALL: ");
                        String answer = scan.nextLine();
                        switch(answer){
                            case "ALL":skipask=true;
                                break;
                            case "n":
                            case "N":return;
                            case "Y":break;
                        }
                    }
                    f.renameTo(new File(target.getPath(),result));
                    System.out.println(name +" renamed to "+result);
                                     
                } else if (listOfFiles[i].isDirectory()) {
                    rename(listOfFiles[i],regex,replace);
                }
            }
        } else {
            System.out.println("Path without files.");
        }
    }
}
